package br.com.carrinho.carrinho;


import org.junit.Assert;
import org.junit.Test;

public class ProdutoTest {

	@Test
	public void testEquals() {
		Produto p1 = new Produto(1L, "Produto 1");
		Produto p2 = new Produto(1L, "Produto 2 com código 1");
		Produto p3 = new Produto(3L, "Produto 3");

		Assert.assertTrue(p1.equals(p2));
		Assert.assertFalse(p1.equals(p3));
	}

}
