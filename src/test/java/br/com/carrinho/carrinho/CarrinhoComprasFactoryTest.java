package br.com.carrinho.carrinho;


import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CarrinhoComprasFactoryTest {

	private CarrinhoComprasFactory carrinhoFactory;

	@Before
	public void setUp() {
		carrinhoFactory = new CarrinhoComprasFactory();
	}

	@Test
	public void testCriar() {
		CarrinhoCompras c1 = carrinhoFactory.criar("123");

		Assert.assertEquals(1L, carrinhoFactory.getCarrinhos().size());

		Assert.assertTrue(carrinhoFactory.getCarrinhos().contains(c1));

		carrinhoFactory.criar("123");
		Assert.assertEquals(1L, carrinhoFactory.getCarrinhos().size());

		carrinhoFactory.criar("456");
		Assert.assertEquals(2L, carrinhoFactory.getCarrinhos().size());
	}

	@Test
	public void testGetValorTicketMedio() {
		Assert.assertEquals(BigDecimal.valueOf(0.00).setScale(2), carrinhoFactory
				.getValorTicketMedio());

		Produto p1 = new Produto(1L, "Produto 1");

		carrinhoFactory.criar("123").adicionarItem(p1, BigDecimal.TEN, 2);
		Assert.assertEquals(BigDecimal.valueOf(20).setScale(2), carrinhoFactory
				.getValorTicketMedio());

		carrinhoFactory.criar("456").adicionarItem(p1, BigDecimal.TEN, 8);
		Assert.assertEquals(BigDecimal.valueOf(50).setScale(2), carrinhoFactory
				.getValorTicketMedio());

		carrinhoFactory.criar("456").adicionarItem(p1, BigDecimal.valueOf(10.10), 1);
		Assert.assertEquals(BigDecimal.valueOf(55.45).setScale(2), carrinhoFactory
				.getValorTicketMedio());

		carrinhoFactory.criar("456").adicionarItem(p1, BigDecimal.valueOf(1.05), 1);
		Assert.assertEquals(BigDecimal.valueOf(15.25).setScale(2), carrinhoFactory
				.getValorTicketMedio());

	}

	@Test
	public void testInvalidar() {
		Assert.assertFalse(carrinhoFactory.invalidar("123"));

		carrinhoFactory.criar("123");

		Assert.assertEquals(1L, carrinhoFactory.getCarrinhos().size());
		Assert.assertNotNull(carrinhoFactory.busca("123"));

		Assert.assertTrue(carrinhoFactory.invalidar("123"));

		Assert.assertEquals(0L, carrinhoFactory.getCarrinhos().size());
		Assert.assertNull(carrinhoFactory.busca("123"));

	}

}
