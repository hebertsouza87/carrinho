package br.com.carrinho.carrinho;


import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.carrinho.exception.ObjetoNuloException;

public class CarrinhoComprasTest {

	private CarrinhoCompras carrinho;
	Produto p1 = new Produto(1L, "Produto 1");
	Produto p2 = new Produto(2L, "Produto 2");
	Produto p3 = new Produto(3L, "Produto 3");

	@Before
	public void setUp() {
		carrinho = new CarrinhoCompras("01");
	}

	@Test
	public void testAdicionarItem() {
		carrinho.adicionarItem(p1, BigDecimal.TEN, 1);
		Assert.assertEquals(1, carrinho.getItens().size());
		Assert.assertEquals(BigDecimal.TEN, ((List<Item>) carrinho.getItens()).get(0)
				.getValorUnitario());

		carrinho.adicionarItem(p1, BigDecimal.ONE, 1);
		Assert.assertEquals(BigDecimal.ONE, ((List<Item>) carrinho.getItens()).get(0)
				.getValorUnitario());

		carrinho.adicionarItem(p2, BigDecimal.valueOf(3), 5);
		Assert.assertEquals(2, carrinho.getItens().size());

		try {
			carrinho.adicionarItem(null, BigDecimal.valueOf(3), 5);
			Assert.fail("Deveria ter lançado ObjetoNuloException");
		} catch (ObjetoNuloException e) {
			Assert
					.assertEquals("Não é possível adicionar um item com produto nulo!", e
							.getMessage());
		} catch (Exception e) {
			Assert.fail("Deveria ter lançado ObjetoNuloException");
		}
	}

	@Test
	public void testGetValorTotal() {
		Item i1 = new Item(p1, BigDecimal.TEN, 1);
		Item i2 = new Item(p2, BigDecimal.valueOf(3), 5);
		Item i3 = new Item(p3, BigDecimal.valueOf(3.56), 1);

		carrinho.getItens().add(i1);
		Assert.assertEquals(BigDecimal.TEN, carrinho.getValorTotal());

		carrinho.getItens().add(i2);
		Assert.assertEquals(BigDecimal.valueOf(25), carrinho.getValorTotal());

		carrinho.getItens().add(i3);
		Assert.assertEquals(BigDecimal.valueOf(28.56), carrinho.getValorTotal());
	}

	@Test
	public void testRemoverItem_Produto() {
		Item i1 = new Item(p1, BigDecimal.valueOf(1), 1);
		Item i2 = new Item(p2, BigDecimal.valueOf(2), 2);
		Item i3 = new Item(p3, BigDecimal.valueOf(3), 3);

		carrinho.getItens().add(i1);
		carrinho.getItens().add(i2);
		carrinho.getItens().add(i3);

		Assert.assertTrue(carrinho.getItens().contains(i1));
		Assert.assertTrue(carrinho.getItens().contains(i2));
		Assert.assertTrue(carrinho.getItens().contains(i3));

		carrinho.removerItem(p1);

		Assert.assertEquals(3, carrinho.getItens().size());
		Assert.assertFalse(carrinho.getItens().contains(i1));
		Assert.assertTrue(carrinho.getItens().contains(i2));
		Assert.assertTrue(carrinho.getItens().contains(i3));
	}

	@Test
	public void testRemoverItem_int() {
		Item i1 = new Item(p1, BigDecimal.valueOf(1), 1);
		Item i2 = new Item(p2, BigDecimal.valueOf(2), 2);
		Item i3 = new Item(p3, BigDecimal.valueOf(3), 3);

		carrinho.getItens().add(i1);
		carrinho.getItens().add(i2);
		carrinho.getItens().add(i3);

		Assert.assertTrue(carrinho.getItens().contains(i1));
		Assert.assertTrue(carrinho.getItens().contains(i2));
		Assert.assertTrue(carrinho.getItens().contains(i3));

		Assert.assertTrue(carrinho.removerItem(0));

		Assert.assertEquals(3, carrinho.getItens().size());
		Assert.assertFalse(carrinho.getItens().contains(i1));
		Assert.assertTrue(carrinho.getItens().contains(i2));
		Assert.assertTrue(carrinho.getItens().contains(i3));

		Assert.assertFalse(carrinho.removerItem(0));
		Assert.assertFalse(carrinho.removerItem(3));
	}

}
