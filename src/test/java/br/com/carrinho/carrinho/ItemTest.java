package br.com.carrinho.carrinho;


import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ItemTest {

	private Item item;

	@Before
	public void setUp() {
		Produto p1 = new Produto(1L, "Produto 1");
		item = new Item(p1, BigDecimal.valueOf(2.25), 3);
	}

	@Test
	public void testGetValorTotal() {
		Assert.assertEquals(BigDecimal.valueOf(6.75), item.getValorTotal());
	}

	@Test
	public void testRepresenta() {
		Produto p2 = new Produto(1L, "Produto 2 para teste");
		Produto p3 = new Produto(3L, "Produto 3 para teste");

		Assert.assertTrue(item.representa(p2));
		Assert.assertFalse(item.representa(p3));
	}

}
