package br.com.carrinho.carrinho;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;

/**
 * Classe responsável pela criação e recuperação dos carrinhos de compras.
 */
public class CarrinhoComprasFactory {

	private final HashSet<CarrinhoCompras> carrinhos = new HashSet<>();

	/**
	 * Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro.
	 * <p>
	 * Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho
	 * deverá ser retornado.
	 *
	 * @param identificacaoCliente
	 * @return CarrinhoCompras
	 */
	public CarrinhoCompras criar(String identificacaoCliente) {
		CarrinhoCompras carrinho = new CarrinhoCompras(identificacaoCliente);
		if (carrinhos.add(carrinho)) {
			return carrinho;
		}
		return carrinhos.stream().filter(c -> c.equals(carrinho)).findFirst().get();
	}

	/**
	 * Retorna o valor do ticket médio no momento da chamada ao método. O valor do ticket médio é a
	 * soma do valor total de todos os carrinhos de compra dividido pela quantidade de carrinhos de
	 * compra. O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:
	 * 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTicketMedio() {
		BigDecimal total = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

		if (carrinhos.isEmpty()) {
			return total;
		}

		for (CarrinhoCompras carrinho : carrinhos) {
			total = total.add(carrinho.getValorTotal());
		}

		return total.divide(BigDecimal.valueOf(carrinhos.size()));
	}

	/**
	 * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar. Deve
	 * ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos
	 * de compras.
	 *
	 * @param identificacaoCliente
	 * @return Retorna um boolean, tendo o valor true caso o cliente passado como parämetro tenha um
	 *         carrinho de compras e e false caso o cliente não possua um carrinho.
	 */
	public boolean invalidar(String identificacaoCliente) {
		return carrinhos.removeIf(c -> c.getCliente().equals(identificacaoCliente));
	}

	public CarrinhoCompras busca(String identificacaoCliente) {
		return carrinhos.stream().filter(c -> c.getCliente().equals(identificacaoCliente))
				.findFirst().orElse(null);
	}

	public HashSet<CarrinhoCompras> getCarrinhos() {
		return carrinhos;
	}
}
