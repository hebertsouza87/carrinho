package br.com.carrinho.carrinho;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import br.com.carrinho.exception.ObjetoNuloException;

/**
 * Classe que representa o carrinho de compras de um cliente.
 */
public class CarrinhoCompras {

	private final String cliente;
	private final List<Item> itens = new ArrayList<>();

	public CarrinhoCompras(String cliente) {
		this.cliente = cliente;
	}

	/**
	 * Permite a adição de um novo item no carrinho de compras.
	 * <p>
	 * Caso o item já exista no carrinho para este mesmo produto, as seguintes regras deverão ser
	 * seguidas: - A quantidade do item deverá ser a soma da quantidade atual com a quantidade
	 * passada como parâmetro. - Se o valor unitário informado for diferente do valor unitário atual
	 * do item, o novo valor unitário do item deverá ser o passado como parâmetro.
	 * <p>
	 * Devem ser lançadas subclasses de RuntimeException caso não seja possível adicionar o item ao
	 * carrinho de compras.
	 *
	 * @param produto
	 * @param valorUnitario
	 * @param quantidade
	 */
	public void adicionarItem(Produto produto, BigDecimal valorUnitario, int quantidade) {
		if (produto == null) {
			throw new ObjetoNuloException("Não é possível adicionar um item com produto nulo!");
		}
		for (int i = 0; i < itens.size(); i++) {
			Item item = itens.get(i);
			if (item.representa(produto)) {
				item.setQuantidade(item.getQuantidade() + quantidade);
				item.setValorUnitario(valorUnitario);
				itens.set(i, item);
				return;
			}
		}
		itens.add(new Item(produto, valorUnitario, quantidade));
	}

	/**
	 * Permite a remoção do item que representa este produto do carrinho de compras.
	 *
	 * @param produto
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e
	 *         false caso o produto não exista no carrinho.
	 */
	public boolean removerItem(Produto produto) {
		if (produto == null) {
			return false;
		}

		for (int i = 0; i < itens.size(); i++) {
			Item item = itens.get(i);
			if (item.representa(produto)) {
				itens.set(i, null);
				return true;
			}
		}

		return false;
	}

	/**
	 * Permite a remoção do item de acordo com a posição. Essa posição deve ser determinada pela
	 * ordem de inclusão do produto na coleção, em que zero representa o primeiro item.
	 *
	 * @param posicaoItem
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no carrinho de compras e
	 *         false caso o produto não exista no carrinho.
	 */
	public boolean removerItem(int posicaoItem) {
		if (itens.size() - 1 < posicaoItem || itens.get(posicaoItem) == null) {
			return false;
		}

		itens.set(posicaoItem, null);
		return true;
	}

	/**
	 * Retorna o valor total do carrinho de compras, que deve ser a soma dos valores totais de todos
	 * os itens que compõem o carrinho.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTotal() {
		BigDecimal valorTotal = BigDecimal.ZERO;
		for (Item item : itens) {
			if (item != null) {
				valorTotal = valorTotal.add(item.getValorTotal());
			}
		}
		return valorTotal;
	}

	/**
	 * Retorna a lista de itens do carrinho de compras.
	 *
	 * @return itens
	 */
	public Collection<Item> getItens() {
		return itens;
	}

	public String getCliente() {
		return cliente;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final CarrinhoCompras other = (CarrinhoCompras) obj;
		return Objects.equals(this.cliente, other.cliente);
	}

}
