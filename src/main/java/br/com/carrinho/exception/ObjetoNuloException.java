package br.com.carrinho.exception;

public class ObjetoNuloException extends RuntimeException {
	private static final long serialVersionUID = 1355807250891092938L;

	public ObjetoNuloException(String message) {
		super(message);
	}

}
